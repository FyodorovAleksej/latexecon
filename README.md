# LatexEcon

## Edit to yours:

### [.gitlab-ci.yml](./.gitlab-ci.yml#L13):

edit `/doc` to your path to directory with Makefile and others documents
```
 - docker run -v $(pwd)/doc:/doc -w /doc ctex
```

For example:
```
 - docker run -v $(pwd)/documents:/documents -w /documents ctex
```


### [doc/diploma_content.tex](./doc/diploma_content.tex#L2):

edit `\setcounter{page}{25}` to your start page number for economic part
For example:
```
\setcounter{page}{67}
```

### [doc/sec_econ.tex 1](./doc/sec_econ.tex#L24):

edit this:
```
\FPeval{\valq}{1900}%%%%%%%%%%%%%%%%%%%%%%%%%
```
to your count of operators (lines of code).

### [doc/sec_econ.tex 2](./doc/sec_econ.tex#L155):

**And also change description of your own product.**

Thanks and good luck :)




